package de.set.jira.xmpp.config;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Strings;

public class XmppServerConfig {

    private static final String XMPP_SERVER = "xmpp-server";
    private static final String XMPP_PORT = "xmpp-port";
    private static final String XMPP_USER = "xmpp-user";
    private static final String XMPP_PASSWORD = "xmpp-password";

    private final PluginSettings settings;

    public XmppServerConfig(final PluginSettingsFactory settingsFactory) {
        this.settings = settingsFactory.createGlobalSettings();
    }

    public boolean isConfigSet() {
        return this.getUser() != null;
    }

    public String getUser() {
        return (String) this.settings.get(XMPP_USER);
    }

    public void setUser(final String user) {
        this.settings.put(XMPP_USER, user);
    }

    public String getPassword() {
        return (String) this.settings.get(XMPP_PASSWORD);
    }

    public void setPassword(final String password) {
        this.settings.put(XMPP_PASSWORD, password);
    }

    public String getServer() {
        return (String) this.settings.get(XMPP_SERVER);
    }

    public void setServer(final String server) {
        this.settings.put(XMPP_SERVER, server);
    }

    public Integer getPort() {
        final String port = (String) this.settings.get(XMPP_PORT);
        if (Strings.isNullOrEmpty(port)) {
            return null;
        }
        return Integer.parseInt(port);
    }

    public void setPort(final Integer port) {
        final String portString = port == null ? null : port.toString();
        this.settings.put(XMPP_PORT, portString);
    }

}
