package de.set.jira.xmpp.admin;

import java.util.Collection;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;

import de.set.jira.xmpp.event.EventReceiverTypeManager;
import de.set.jira.xmpp.event.EventRule;
import de.set.jira.xmpp.event.EventRuleManager;

@SuppressWarnings("serial")
public class EditEventRule extends AbstractEventRuleAction {

    private int eventRuleId;
    private final EventRuleManager eventRuleManager;
    private String message;
    private long eventTypeId;
    private final EventTypeManager eventTypeManager;

    public EditEventRule(final EventRuleManager eventRuleManager,
            final EventReceiverTypeManager receiverTypeManager,
            final EventTypeManager eventTypeManager) {
        super(receiverTypeManager);
        this.eventRuleManager = eventRuleManager;
        this.eventTypeManager = eventTypeManager;
    }

    public String doShow() {
        return SUCCESS;
    }

    public String doSubmit() {
        final EventRule eventRule = this.getEventRule();
        eventRule.setEventType(this.eventTypeManager
                .getEventType(this.eventTypeId));
        eventRule.setMessage(this.message);

        this.eventRuleManager.updateEventRule(eventRule);

        return this.returnCompleteWithInlineRedirect("EventRulesOverview.jspa");
    }

    public void setEventRuleId(final int eventRuleId) {
        this.eventRuleId = eventRuleId;
    }

    public void setEventTypeId(final long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Collection<EventType> getEventTypes() {
        return this.eventTypeManager.getEventTypes();
    }

    public EventRule getEventRule() {
        return this.eventRuleManager.getEventRule(this.eventRuleId);
    }
}
