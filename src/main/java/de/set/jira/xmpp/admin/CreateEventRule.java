package de.set.jira.xmpp.admin;

import java.util.Collection;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import de.set.jira.xmpp.event.EventRule;
import de.set.jira.xmpp.event.EventRuleManager;

@SuppressWarnings("serial")
public class CreateEventRule extends JiraWebActionSupport {

    private EventTypeManager eventTypeManager;
    private EventRuleManager eventRuleManager;
    private long eventTypeId;
    
    CreateEventRule(EventTypeManager eventTypeManager,
            EventRuleManager eventRuleManager) {
        super();
        this.eventTypeManager = eventTypeManager;
        this.eventRuleManager = eventRuleManager;
    }

    @Override
    protected String doExecute() throws Exception {
        EventRule eventRule = this.eventRuleManager.createEventRule();
        eventRule.setEventType(eventTypeManager.getEventType(eventTypeId));
        this.eventRuleManager.updateEventRule(eventRule);
        
        return this.returnCompleteWithInlineRedirect("EventRulesOverview.jspa");
    }
    
    @Override
    protected void doValidation() {
        try {
            this.eventTypeManager.getEventType(eventTypeId);
        } catch (IllegalArgumentException e) {
            addError("eventTypeId", "No valid event type selected");
        }
        super.doValidation();
    }
    
    public String doShowDialog() {
        return SUCCESS;
    }
    
    public void setEventTypeId(long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
    
    public Collection<EventType> getEventTypes() {
        return this.eventTypeManager.getEventTypes();
    }
}
