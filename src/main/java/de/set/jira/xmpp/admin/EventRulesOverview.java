package de.set.jira.xmpp.admin;

import java.util.Collection;

import de.set.jira.xmpp.event.EventReceiverTypeManager;
import de.set.jira.xmpp.event.EventRule;
import de.set.jira.xmpp.event.EventRuleManager;

@SuppressWarnings("serial")
public class EventRulesOverview extends AbstractEventRuleAction {

    private final EventRuleManager ruleManager;

    EventRulesOverview(final EventRuleManager ruleManager,
            final EventReceiverTypeManager receiverTypeManager) {
        super(receiverTypeManager);
        this.ruleManager = ruleManager;
    }

    @Override
    protected String doExecute() throws Exception {
        return SUCCESS;
    }

    public Collection<EventRule> getEventRules() {
        return this.ruleManager.getAllEventRules();
    }

}
