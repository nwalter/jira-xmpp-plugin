package de.set.jira.xmpp.admin;

import java.util.List;

import webwork.action.ResultException;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Strings;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.event.EventReceiverTypeModuleDescriptor;
import de.set.jira.xmpp.event.EventRule;
import de.set.jira.xmpp.event.EventRuleManager;

@SuppressWarnings("serial")
public class CreateEventReceiver extends JiraWebActionSupport {
    
    private int ruleId;
    private int receiverId;
    private EventRuleManager eventRuleManager;
    private String receiverType;
    private String receiver;
    private PluginAccessor pluginAccessor;

    public CreateEventReceiver(EventRuleManager eventRuleManager, PluginAccessor pluginAccessor) {
        super();
        this.eventRuleManager = eventRuleManager;
        this.pluginAccessor = pluginAccessor;
    }
    
    public String doDelete() {
        this.eventRuleManager.removeReceiver(this.receiverId);
        
        return  this.returnCompleteWithInlineRedirect("ShowEditEventRule.jspa?eventRuleId=" + this.ruleId);
    }
    
    public String doSubmit() throws ResultException {
        validate();
        EventRule eventRule = this.getEventRule();
        EventReceiver receiverRule = this.eventRuleManager.createEventReceiverRule();
        receiverRule.setReceiver(receiver);
        receiverRule.setRuleType(receiverType);
        eventRule.addReceiver(receiverRule);
        
        this.eventRuleManager.updateEventRule(eventRule);
        
        return  this.returnCompleteWithInlineRedirect("ShowEditEventRule.jspa?eventRuleId=" + this.ruleId);
    }
    
    @Override
    protected void doValidation() {
        if (Strings.isNullOrEmpty(receiverType)) {
            addError("receiverType", "No known receiver type");
        }
    }
    
    public String doAdd() {
        return SUCCESS;
    }
    
    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }
    
    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }
    
    public List<EventReceiverTypeModuleDescriptor> getReceiverTypes() {
        return this.pluginAccessor.getEnabledModuleDescriptorsByClass(EventReceiverTypeModuleDescriptor.class);
    }
    
    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }
    
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
    
    public EventRule getEventRule() {
        return this.eventRuleManager.getEventRule(this.ruleId);
    }

}
