package de.set.jira.xmpp.admin;

import com.atlassian.jira.web.action.JiraWebActionSupport;

import de.set.jira.xmpp.event.EventRuleManager;

@SuppressWarnings("serial")
public class DeleteEventRule extends JiraWebActionSupport {


    private int eventRuleId;
    private EventRuleManager eventRuleManager;
    
    public DeleteEventRule(EventRuleManager eventRuleManager) {
        super();
        this.eventRuleManager = eventRuleManager;
    }
    
    @Override
    protected String doExecute() throws Exception {
        this.eventRuleManager.deleteRule(this.eventRuleId);
        return this.returnCompleteWithInlineRedirect("EventRulesOverview.jspa");
    }
    
    public void setEventRuleId(int eventRuleId) {
        this.eventRuleId = eventRuleId;
    }
}
