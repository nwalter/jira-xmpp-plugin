package de.set.jira.xmpp.admin;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.google.common.base.Strings;

import de.set.jira.xmpp.api.XmppClient;
import de.set.jira.xmpp.config.XmppServerConfig;

@SuppressWarnings("serial")
public class EditXmppServerConfig extends JiraWebActionSupport {

    private final XmppServerConfig config;
    private final XmppClient client;
    private String password;
    private Integer port;
    private String user;
    private String server;

    public EditXmppServerConfig(final XmppServerConfig config,
            final XmppClient xmppClient) {
        this.client = xmppClient;
        this.config = config;
    }

    @Override
    protected String doExecute() throws Exception {

        this.config.setUser(this.user);
        this.config.setPassword(this.password);
        this.config.setServer(this.server);
        this.config.setPort(this.port);

        this.client.connect();

        return this
                .returnCompleteWithInlineRedirect("ShowXmppServerConfig.jspa");
    }

    public String doShow() {
        return SUCCESS;
    }

    @Override
    protected void doValidation() {
        if (Strings.isNullOrEmpty(this.user)) {
            this.addError("user", "Is empty");
        }

        if (Strings.isNullOrEmpty(this.password)) {
            this.addError("password", "Is empty");
        }

        if (this.port != null && this.port < 0) {
            this.addError("port", "Port has to be greater than 0");
        }

        super.doValidation();
    }

    public String getServer() {
        return this.config.getServer();
    }

    public Integer getPort() {
        return this.config.getPort();
    }

    public String getUser() {
        return this.config.getUser();
    }

    public String getPassword() {
        return this.config.getPassword();
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setServer(final String server) {
        this.server = server;
    }

    public void setPort(final Integer port) {
        this.port = port;
    }

    public void setUser(final String user) {
        this.user = user;
    }
}
