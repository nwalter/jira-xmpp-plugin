package de.set.jira.xmpp.admin;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.google.common.base.Optional;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.event.EventReceiverTypeManager;
import de.set.jira.xmpp.event.EventReceiverTypeModuleDescriptor;

@SuppressWarnings("serial")
public abstract class AbstractEventRuleAction extends JiraWebActionSupport {

    private final EventReceiverTypeManager receiverTypeManager;

    public AbstractEventRuleAction(
            final EventReceiverTypeManager receiverTypeManager) {
        super();
        this.receiverTypeManager = receiverTypeManager;
    }

    public String describe(final EventReceiver receiver) {

        final Optional<EventReceiverTypeModuleDescriptor> moduleDescriptor = this.receiverTypeManager
                .getDescriptorForKey(receiver.getType());

        if (moduleDescriptor.isPresent()) {
            return moduleDescriptor.get().getModule()
                    .getActualDescription(receiver);
        } else {
            return "Unknown receiver";
        }
    }

}