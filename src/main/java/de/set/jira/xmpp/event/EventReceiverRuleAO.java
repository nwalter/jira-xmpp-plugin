package de.set.jira.xmpp.event;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("RcvrRule")
public interface EventReceiverRuleAO extends Entity {

    String getType();
    
    String getReceiver();

    void setType(String type);

    void setReceiver(String receiver);
    
    EventRuleAO getRule();
    
    void setRule(EventRuleAO rule);
    
}
