package de.set.jira.xmpp.event.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;

import de.set.jira.xmpp.api.XmppPluginException;
import de.set.jira.xmpp.event.EventRuleManager;

public class XmppEventListener implements InitializingBean, DisposableBean {

    private static Logger log = LoggerFactory
            .getLogger(XmppEventListener.class);
    private final EventRuleManager ruleManager;
    private final EventPublisher eventPublisher;

    XmppEventListener(final EventPublisher eventPublisher,
            final EventRuleManager ruleManager) {
        this.eventPublisher = eventPublisher;
        this.ruleManager = ruleManager;
        eventPublisher.register(this);
    }

    @EventListener
    public void onIssueEvent(final IssueEvent issueEvent) {
        final Issue issue = issueEvent.getIssue();

        if (issueEvent.isSendMail()) {
            log.info("Issue {} was target in event type: {}", issue.getKey(),
                    issueEvent);
            try {
                this.ruleManager.publish(issueEvent);
            } catch (final XmppPluginException e) {
                log.error("Not able to publish event to xmpp", e);
            }
        }
    }

    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.eventPublisher.register(this);
    }

}
