package de.set.jira.xmpp.event;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

import de.set.jira.xmpp.api.EventReceiverType;

public class EventReceiverTypeModuleDescriptor extends
        AbstractModuleDescriptor<EventReceiverType> {

    public EventReceiverTypeModuleDescriptor(final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public EventReceiverType getModule() {
        return this.moduleFactory.createModule(this.moduleClassName, this);
    }

}
