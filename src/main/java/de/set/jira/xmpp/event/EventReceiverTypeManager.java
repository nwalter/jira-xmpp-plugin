package de.set.jira.xmpp.event;

import java.util.Collection;

import com.google.common.base.Optional;

public interface EventReceiverTypeManager {

    Collection<EventReceiverTypeModuleDescriptor> getAllDescriptors();

    Optional<EventReceiverTypeModuleDescriptor> getDescriptorForKey(String key);
}
