package de.set.jira.xmpp.event;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.api.EventReceiverType;
import de.set.jira.xmpp.api.XmppPluginException;

public class EventRuleManagerImpl implements EventRuleManager {

    private static final Logger log = LoggerFactory
            .getLogger(EventRuleManagerImpl.class);

    private final Function<EventRuleAO, EventRule> mapEventRuleAO = new Function<EventRuleAO, EventRule>() {

        @Override
        public EventRule apply(final EventRuleAO ao) {
            final int id = ao.getID();
            final String message = ao.getMessage();
            EventType eventType = null;
            try {
                eventType = ComponentAccessor.getEventTypeManager()
                        .getEventType(ao.getEventTypeId());
            } catch (final IllegalArgumentException e) {
            }
            final List<EventReceiver> receivers = Lists.transform(
                    Lists.newArrayList(ao.getEventReceiverRules()),
                    EventRuleManagerImpl.this.mapEventReceiverAO);

            return new EventRule(id, eventType, message, receivers);
        }
    };

    private final Function<EventReceiverRuleAO, EventReceiver> mapEventReceiverAO = new Function<EventReceiverRuleAO, EventReceiver>() {

        @Override
        public EventReceiver apply(final EventReceiverRuleAO ao) {
            final int id = ao.getID();
            final String receiver = ao.getReceiver();
            return new EventReceiver(id, receiver, ao.getType());
        }
    };

    private final ActiveObjects activeObjects;

    private final EventReceiverTypeManager receiverTypeManager;

    EventRuleManagerImpl(final ActiveObjects activeObjects,
            final EventReceiverTypeManager receiverTypeManager) {
        super();
        this.activeObjects = activeObjects;
        this.receiverTypeManager = receiverTypeManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#getAllEventRules()
     */
    @Override
    public Collection<EventRule> getAllEventRules() {
        final EventRuleAO[] ruleAOs = this.activeObjects
                .find(EventRuleAO.class);
        return FluentIterable.from(Lists.newArrayList(ruleAOs))
                .transform(this.mapEventRuleAO).toList();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.set.jira.xmpp.event.EventRuleManager#updateEventRule(de.set.jira.xmpp
     * .event.EventRule)
     */
    @Override
    public void updateEventRule(final EventRule eventRule) {
        final EventRuleAO ruleAO = this.activeObjects.get(EventRuleAO.class,
                eventRule.getID());
        ruleAO.setEventTypeId(eventRule.getEventType().getId());
        ruleAO.setMessage(eventRule.getMessage());
        for (final EventReceiver receiverRule : eventRule.getReceivers()) {
            this.updateReceiverRule(ruleAO, receiverRule);
        }
        ruleAO.save();
    }

    private void updateReceiverRule(final EventRuleAO eventRule,
            final EventReceiver eventReceiverRule) {
        final EventReceiverRuleAO ruleAO = this.activeObjects.get(
                EventReceiverRuleAO.class, eventReceiverRule.getID());
        ruleAO.setReceiver(eventReceiverRule.getReceiver());
        ruleAO.setRule(eventRule);
        ruleAO.setType(eventReceiverRule.getType());

        ruleAO.save();
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#getRulesForEvent(long)
     */
    @Override
    public Collection<EventRule> getRulesForEvent(final long eventTypeId) {
        return FluentIterable.from(this.getAllEventRules())
                .filter(this.forEventType(eventTypeId)).toList();
    }

    private Predicate<? super EventRule> forEventType(final long eventTypeId) {
        return new Predicate<EventRule>() {

            @Override
            public boolean apply(final EventRule rule) {
                return rule.getEventType().getId().equals(eventTypeId);
            }
        };
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * de.set.jira.xmpp.event.EventRuleManager#publish(com.atlassian.jira.event
     * .issue.IssueEvent)
     */
    @Override
    public void publish(final IssueEvent issueEvent) throws XmppPluginException {
        final Collection<EventRule> rulesForEvent = this
                .getRulesForEvent(issueEvent.getEventTypeId());
        for (final EventRule eventRule : rulesForEvent) {
            for (final EventReceiver receiver : eventRule.getReceivers()) {
                this.notifyReceiver(receiver, issueEvent, eventRule);
            }
        }
    }

    private void notifyReceiver(final EventReceiver receiver,
            final IssueEvent issueEvent, final EventRule eventRule)
                    throws XmppPluginException {
        final String message = eventRule.getMessage();
        final String key = receiver.getType();

        final Optional<EventReceiverTypeModuleDescriptor> descriptorForKey = this.receiverTypeManager
                .getDescriptorForKey(key);

        if (!descriptorForKey.isPresent()) {
            log.warn(
                    "Cannot find receiver type {} for rule {}, may be the containing plugin was disables/removed",
                    key, eventRule.getID());
            return;
        }

        final EventReceiverType receiverType = descriptorForKey.get()
                .getModule();
        receiverType.publishEvent(receiver, issueEvent, message);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#createEventRule()
     */
    @Override
    public EventRule createEventRule() {
        final EventRuleAO ruleAO = this.activeObjects.create(EventRuleAO.class);
        final EventRule rule = this.mapEventRuleAO.apply(ruleAO);

        return rule;
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#createEventReceiverRule()
     */
    @Override
    public EventReceiver createEventReceiverRule() {
        final EventReceiverRuleAO ruleAO = this.activeObjects
                .create(EventReceiverRuleAO.class);
        return this.mapEventReceiverAO.apply(ruleAO);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#deleteRule(int)
     */
    @Override
    public void deleteRule(final int eventRuleId) {
        final EventRuleAO ruleAO = this.activeObjects.get(EventRuleAO.class,
                eventRuleId);
        final EventReceiverRuleAO[] eventReceiverRules = ruleAO
                .getEventReceiverRules();
        this.activeObjects.delete(ruleAO);
        this.activeObjects.delete(eventReceiverRules);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#getEventRule(int)
     */
    @Override
    public EventRule getEventRule(final int eventRuleId) {
        final EventRuleAO ruleAO = this.activeObjects.get(EventRuleAO.class,
                eventRuleId);
        return this.mapEventRuleAO.apply(ruleAO);
    }

    /*
     * (non-Javadoc)
     * 
     * @see de.set.jira.xmpp.event.EventRuleManager#removeReceiver(int)
     */
    @Override
    public void removeReceiver(final int receiverID) {
        final EventReceiverRuleAO receiverRuleAO = this.activeObjects.get(
                EventReceiverRuleAO.class, receiverID);
        this.activeObjects.delete(receiverRuleAO);
    }

}
