package de.set.jira.xmpp.event.types;

import com.atlassian.jira.event.issue.IssueEvent;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.api.EventReceiverType;
import de.set.jira.xmpp.api.XmppClient;
import de.set.jira.xmpp.api.XmppPluginException;

public class JIDReceiverType implements EventReceiverType {

    private final XmppClient xmppClient;

    public JIDReceiverType(final XmppClient xmppClient) {
        this.xmppClient = xmppClient;
    }

    @Override
    public void publishEvent(final EventReceiver receiver,
            final IssueEvent event, final String message)
                    throws XmppPluginException {
        this.xmppClient.sendMessage(receiver.getReceiver(), message);
    }

    @Override
    public String getActualDescription(final EventReceiver receiverRule) {
        return new StringBuilder("Send message to ").append(
                receiverRule.getReceiver()).toString();
    }

}
