package de.set.jira.xmpp.event;

import java.util.Collection;

import com.atlassian.jira.event.issue.IssueEvent;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.api.XmppPluginException;

public interface EventRuleManager {

    public abstract Collection<EventRule> getAllEventRules();

    public abstract void updateEventRule(EventRule eventRule);

    public abstract Collection<EventRule> getRulesForEvent(long eventTypeId);

    public abstract void publish(IssueEvent issueEvent)
            throws XmppPluginException;

    public abstract EventRule createEventRule();

    public abstract EventReceiver createEventReceiverRule();

    public abstract void deleteRule(int eventRuleId);

    public abstract EventRule getEventRule(int eventRuleId);

    public abstract void removeReceiver(int receiverID);

}