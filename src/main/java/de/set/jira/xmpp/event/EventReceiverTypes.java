package de.set.jira.xmpp.event;

import com.atlassian.jira.event.issue.IssueEvent;

import de.set.jira.xmpp.api.EventReceiver;
import de.set.jira.xmpp.api.EventReceiverType;
import de.set.jira.xmpp.api.XmppPluginException;

public enum EventReceiverTypes implements EventReceiverType {
    ASSIGNEE {

        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to the current assignee";
        }

    },
    AUTHOR {
        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to the author of the issue";
        }
    },
    WATCHERS {
        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to all watchers";
        }
    },
    PROJECTROLE {

        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to all user with the rule in the project of the issue";
        }

    },
    GROUP {

        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to all member of a group";
        }

    },
    MUC {

        @Override
        public void publishEvent(final EventReceiver receiver,
                final IssueEvent event, final String message) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getActualDescription(final EventReceiver receiverRule) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getDescription2() {
            return "Send a message to a chatroom";
        }

    };

    /*
     * (non-Javadoc)
     *
     * @see
     * de.set.jira.xmpp.events.EventReceiverType#publishEvent(de.set.jira.xmpp
     * .events.EventReceiver, com.atlassian.jira.event.issue.IssueEvent,
     * de.set.jira.xmpp.client.XmppClient, java.lang.String)
     */
    @Override
    public abstract void publishEvent(EventReceiver receiver, IssueEvent event,
            String message) throws XmppPluginException;

    /*
     * (non-Javadoc)
     *
     * @see
     * de.set.jira.xmpp.events.EventReceiverType#getActualDescription(de.set
     * .jira.xmpp.events.EventReceiver)
     */
    @Override
    public abstract String getActualDescription(EventReceiver receiverRule);

    public abstract String getDescription2();
}