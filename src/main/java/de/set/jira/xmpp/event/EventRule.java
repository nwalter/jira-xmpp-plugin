package de.set.jira.xmpp.event;

import java.util.List;

import com.atlassian.jira.event.type.EventType;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

import de.set.jira.xmpp.api.EventReceiver;

public class EventRule {
    
    private final int id;
    private final List<EventReceiver> receivers;
    private String message;
    private EventType eventType;

    
    EventRule(int id, EventType eventType, String message,
            List<EventReceiver> receivers) {
        super();
        this.id = id;
        this.receivers = Lists.newArrayList(receivers);
        this.message = message;
        this.eventType = eventType;
    }

    public String getMessage() {
        return  this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public void addReceiver(EventReceiver receiverRule) {
        this.receivers.add(receiverRule);
    }
    
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    
    public EventType getEventType() {
        return this.eventType;
    }
    
    public Long getEventTypeId() {
        return this.eventType.getId();
    }
    
    public List<EventReceiver> getReceivers() {
        return receivers; 
    }

    public Integer getID() {
        return this.id;
    }

    public Optional<EventReceiver> getReceiver(final int receiverId) {
        return FluentIterable.from(this.receivers).firstMatch(new Predicate<EventReceiver>() {

            @Override
            public boolean apply(EventReceiver arg0) {
                return arg0.getID() == receiverId;
            }
        });    
    }
}
