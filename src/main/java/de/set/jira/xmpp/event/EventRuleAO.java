package de.set.jira.xmpp.event;

import net.java.ao.Entity;
import net.java.ao.OneToMany;

public interface EventRuleAO extends Entity {

    
    Long getEventTypeId();
    
    String getMessage();
    
    void setMessage(String message);
    
    @OneToMany
    EventReceiverRuleAO[] getEventReceiverRules();

    void setEventTypeId(Long eventTypeId);


}
