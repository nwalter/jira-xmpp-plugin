package de.set.jira.xmpp.event;

import java.util.Collection;

import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

public class EventReceiverTypeManagerImpl implements EventReceiverTypeManager {

    private final PluginAccessor pluginAccessor;

    public EventReceiverTypeManagerImpl(final PluginAccessor pluginAccessor) {
        super();
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public Collection<EventReceiverTypeModuleDescriptor> getAllDescriptors() {
        return this.pluginAccessor
                .getEnabledModuleDescriptorsByClass(EventReceiverTypeModuleDescriptor.class);
    }

    @Override
    public Optional<EventReceiverTypeModuleDescriptor> getDescriptorForKey(
            final String key) {
        return FluentIterable.from(this.getAllDescriptors()).firstMatch(
                new Predicate<EventReceiverTypeModuleDescriptor>() {

                    @Override
                    public boolean apply(
                            final EventReceiverTypeModuleDescriptor input) {
                        return input.getKey().equals(key);
                    }
                });
    }

}
