package de.set.jira.xmpp.client;

import java.io.IOException;

import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.util.XmppStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Strings;

import de.set.jira.xmpp.api.XmppClient;
import de.set.jira.xmpp.api.XmppPluginException;
import de.set.jira.xmpp.config.XmppServerConfig;

public class XmppClientImpl extends AbstractConnectionListener implements
        InitializingBean, DisposableBean, XmppClient {

    private static final String RESSOURCE = "JIRA";
    private static final Logger log = LoggerFactory
            .getLogger(XmppClientImpl.class);
    private static final int XMPP_DEFAULT_PORT = 5222;
    private final XmppServerConfig config;
    private XMPPTCPConnection connection;

    XmppClientImpl(final XmppServerConfig config) {
        this.config = config;
    }

    @Override
    public void connect() throws XmppPluginException {
        this.disconnect();

        final XMPPTCPConnectionConfiguration connectionConfig = this
                .getConnectionConfiguration();
        this.connection = new XMPPTCPConnection(connectionConfig);
        this.connection.addConnectionListener(this);

        try {
            this.connection.connect();
            this.connection.login(this.config.getUser(),
                    this.config.getPassword(), RESSOURCE);
        } catch (final XMPPException | SmackException | IOException e) {
            XmppPluginException.wrap(e);
        }
    }

    @Override
    public void disconnect() {
        if (this.connection != null && this.connection.isConnected()) {
            this.connection.disconnect();
        }
        this.connection = null;
    }

    @Override
    public void destroy() throws Exception {
        log.debug("Disconnectig xmpp for plugin shutdown");
        this.disconnect();
    }

    @Override
    public void afterPropertiesSet() {
        if (this.config.isConfigSet()) {
            log.debug("Connecting to xmpp server");
            try {
                this.connect();
            } catch (final XmppPluginException e) {
                log.warn("Failed to connect to xmpp server", e);
            }
        }
    }

    private XMPPTCPConnectionConfiguration getConnectionConfiguration() {
        final String host = Strings.isNullOrEmpty(this.config.getServer()) ? this
                .getServerFromUser() : this.config.getServer();

        final int port = this.config.getPort() != null ? this.config.getPort()
                : XMPP_DEFAULT_PORT;

        final XMPPTCPConnectionConfiguration connectionConfig =
                XMPPTCPConnectionConfiguration.builder().setHost(host).setPort(port).build();

        return connectionConfig;
    }

    private String getServerFromUser() {
        return XmppStringUtils.parseDomain(this.config.getUser());
    }

    @Override
    public void sendMessage(final String receiver, final String message)
            throws XmppPluginException {
        this.ensureConnected();

        log.info("Sending xmpp message to {}: {}", receiver, message);

        final Message chatMessage = new Message(receiver);
        chatMessage.setType(Message.Type.chat);
        chatMessage.setBody(message);

        try {
            this.connection.sendStanza(chatMessage);
        } catch (final NotConnectedException e) {
            XmppPluginException.wrap(e);
		}
    }

    private void ensureConnected() throws XmppPluginException {
        if (!this.config.isConfigSet()) {
            throw new XmppPluginException("XMPP is not configured");
        } else {
            if (this.connection == null || !this.connection.isConnected()) {
                this.connect();
            }
        }
    }

}
