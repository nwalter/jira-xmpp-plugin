package de.set.jira.xmpp.api;



public class EventReceiver {

    private final int iD;
    private String receiver;
    private String receiverType;

    public EventReceiver(int id, String receiver,
            String receiverType) {
        this.iD = id;
        this.receiver = receiver;
        this.receiverType = receiverType;
    }

    public int getID() {
        return iD;
    }
    
    public String getReceiver() {
        return receiver;
    }
    
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getType() {
        return this.receiverType;
    }
    
    public void setRuleType(String receiverType) {
        this.receiverType = receiverType;
    }

}
