package de.set.jira.xmpp.api;

import com.atlassian.jira.event.issue.IssueEvent;

public interface EventReceiverType {

    public abstract void publishEvent(EventReceiver receiver, IssueEvent event,
            String message) throws XmppPluginException;

    public abstract String getActualDescription(EventReceiver receiverRule);

}