package de.set.jira.xmpp.api;

public class XmppPluginException extends Exception {

    private static final long serialVersionUID = -8598805353789714058L;

    public XmppPluginException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public XmppPluginException(final Throwable cause) {
        super(cause);
    }

    public XmppPluginException(final String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public static XmppPluginException wrap(final Throwable cause)
            throws XmppPluginException {
        throw new XmppPluginException(cause);
    }
}
