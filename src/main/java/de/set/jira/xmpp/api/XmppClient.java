package de.set.jira.xmpp.api;

public interface XmppClient {

    public abstract void connect() throws XmppPluginException;

    public abstract void disconnect();

    public abstract void sendMessage(String receiver, String message)
            throws XmppPluginException;

}