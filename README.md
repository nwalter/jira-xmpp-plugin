# JIRA XMPP Plugin #

The JIRA XMPP plugin extends Atlassians issue tracking software JIRA with the capability to send notifications via the XMPP protocol.

It began as an Ship It Day project within the [SET GmbH](http://www.set.de).

## Maintainer: ##
Niklas Walter <nw@set.de>
